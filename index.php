    <?php

    require "animal.php";
    require "frog.php";
    require "ape.php";


    $sheep = new animal("Shaun");
    //  var_dump("shaun");
    echo $sheep->name . "<br>";
    echo $sheep->leg . "<br>";
    echo var_dump($sheep->cold_blooded) . "<br><br>";

    $kodok = new frog("Buduk");
    echo $kodok->name . "<br>";
    echo $kodok->leg . "<br>";
    echo var_dump($kodok->cold_blooded) . "<br>";
    echo $kodok->jump() . "<br> <br>";


    // NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded()
    $sungokong = new ape("Kerasakti");
    echo $sungokong->name . "<br>";
    echo $sungokong->leg . "<br>";
    echo var_dump($sungokong->cold_blooded) . "<br>";
    echo $sungokong->yel();



    ?>